package com.hcl.restaurant;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Date time = new Date();
		List<Bill> bills = new ArrayList<Bill>();
		boolean finalOrder;
		List<Menu> items = new ArrayList<Menu>();

		Menu item1 = new Menu(1, "Rajma Chawal", 2, 150.0);
		Menu item2 = new Menu(2, "Momos", 2, 190.0);
		Menu item3 = new Menu(3, "Red Thai Curry", 2, 180.0);
		Menu item4 = new Menu(4, "Chaap", 2, 190.0);
		Menu item5 = new Menu(5, "Chilli Potato", 1, 250.0);

		items.add(item1);
		items.add(item2);
		items.add(item3);
		items.add(item4);
		items.add(item5);

		System.out.println("****************WELCOME TO SURABI RESTAURANTS***************");
		System.out.println("=============================================================");
		while (true) {
			System.out.println("Please Enter Your Credentials");

			System.out.println("Email Id:  ");
			String email = input.next();

			System.out.println("Password: ");
			String password = input.next();

			String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);

			System.out.println("Please Enter A if you are Admin and U if you are User, L to logout");
			String aorU = input.next();

			Bill bill = new Bill();
			List<Menu> selectedMenu = new ArrayList<Menu>();

			double totalCost = 0;

			Date date = new Date();

			ZonedDateTime time1 = ZonedDateTime.now();
			DateTimeFormatter datetime = DateTimeFormatter.ofPattern("E MMM dd HH : mm:ss zzz yyyy");
			String currentTime = time1.format(datetime);

			if (aorU.equals("U") || aorU.equals("U")) {
				System.out.println("Welcome " + name);
				// Object selectedItems;
				do {
					System.out.println("Today's Menu : ");
					items.stream().forEach(i -> System.out.println(i));
					System.out.println("Enter the Menu Item code:");
					int code = input.nextInt();

					if (code == 1) {
						selectedMenu.add(item1);
						totalCost += item1.getItemPrice();
					} 
					else if (code == 2) {
						selectedMenu.add(item2);
						totalCost += item2.getItemPrice();
					}
					else if (code == 3) {
						selectedMenu.add(item3);
						totalCost += item3.getItemPrice();
					}
					else if (code == 4) {
						selectedMenu.add(item4);
						totalCost += item4.getItemPrice();
					}
					else {
						selectedMenu.add(item5);
						totalCost += item5.getItemPrice();
					}

					System.out.println("Press 0 to Show Bill");
					System.out.println("Press 1 to Order More");

					int opt = input.nextInt();
					if (opt == 0)
						finalOrder = false;
					else
						finalOrder = true;

				} while (finalOrder);

				System.out.println("Thanks " + name + " for dining in with Surabi ");
				System.out.println("Your Orders Are: ");
				selectedMenu.stream().forEach(e -> System.out.println(e));
				System.out.println("Your Total bill will be " + totalCost);

				bill.setName(name);
				bill.setCost(totalCost);
				bill.setMenu(selectedMenu);
				bill.setTime(date);
				bills.add(bill);
			} else if (aorU.equals("A") || aorU.equals("a")) {
				System.out.println("*Welcome Admin*");
				System.out.println(
						"Press 1 to see all the bills for today\nPress 2 to see all the bills for this month\nPress 3 to see all the bills");
				int option = input.nextInt();
				switch (option) {
				case 1:
					if (!bills.isEmpty()) {
						for (Bill b : bills) {
							if (b.getTime().getDate() == time.getDate()) {
								System.out.println("\nUsername :- " + b.getName());
								System.out.println("Items :- " + b.getMenu());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date " + b.getTime() + "\n");
							}
						}
					} else
						System.out.println("No Bills For Today!");
					break;

				case 2:
					if (!bills.isEmpty()) {
						for (Bill b : bills) {
							if (b.getTime().getMonth() == time.getMonth()) {
								System.out.println("\nUsername :- " + b.getName());
								System.out.println("Items :- " + b.getMenu());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date " + b.getTime() + "\n");
							}
						}
					} else
						System.out.println(" There Are No Bills For This Month!");
					break;

				case 3:
					if (!bills.isEmpty()) {
						for (Bill b : bills) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getMenu());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
					} else
						System.out.println("No Bills Are Available!");

					break;

				default:
					System.out.println("Option Is Invalid");
					System.exit(1);
				}
			} 
			else if (aorU.equals("L") || aorU.equals("l")) {
				System.exit(1);

			} 
			else {
				System.out.println("Invalid Entry");
			}
		}
	}
}
