package com.hcl.restaurant;

public class Menu {
	private int itemID;
	private String itemName;
	private int itemQuantity;
	private double itemPrice;

	public Menu(int ItemID, String ItemName, int ItemQuantity, double ItemPrice) throws IllegalArgumentException {
		super();

		if (ItemID < 0) {
			throw new IllegalArgumentException("Exception occured, ID cannot less than zero");
		}
		this.itemID = ItemID;

		if (ItemName == null) {
			throw new IllegalArgumentException("Exception occured, Name cannot be null");
		}
		this.itemName = ItemName;

		if (ItemQuantity < 0) {
			throw new IllegalArgumentException("Exception occured, Quantity cannot less than zero");
		}
		this.itemQuantity = ItemQuantity;

		if (ItemPrice < 0) {
			throw new IllegalArgumentException("Exception occured, Price cannot less than zero");
		}
		this.itemPrice = ItemPrice;
		}

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "" + itemID + " " + itemName + " " + itemQuantity + " " + itemPrice;
	}

//		return null;
}
